﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbcoApiSample.Models
{
    public class AbcoCartDto
    {
        public int Id { get; set; }

        public int AccountId { get; set; }


        public int ContactId { get; set; }

        public DateTime? DateCreated { get; set; }

        public AbcoAddressDto Address { get; set; }

        public virtual IList<AbcoCartLineDto> Lines { get; set; }

        public double TotalPrice { get; set; }

        public string DeliveryInstructions { get; set; }

        public double? FreightCost { get; set; }

        public double SubTotal { get; set; }

        public double TotalTax { get; set; }

        public string SupplyLocation { get; set; }
    }

    public class AbcoCartLineDto
    {
        public int Quantity { get; set; }

        public double UnitPrice { get; set; }

        public double LineTotal { get; set; }

        public double Cubic { get; set; }

        public double Weight { get; set; }

        public string Stockcode { get; set; }

        public string Description { get; set; }

        public bool Rechargeable { get; set; }

        public string Unspsc { get; set; }
    }
}
