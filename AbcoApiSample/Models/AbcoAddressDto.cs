﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbcoApiSample.Models
{
    public class AbcoAddressDto
    {
        public int Seqno { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public string State { get; set; }

        public string Suburb { get; set; }

        public string Postcode { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string AddressLine3 { get; set; }

        public string AddressLine4 { get; set; }

        public string AddressLine5 { get; set; }

        public string FullAddress { get; set; }
    }
}
