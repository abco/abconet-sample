﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AbcoApiSample.Models
{
    public class AbcoInvoiceDto
    {
        public int InvoiceNo { get; set; }

        public DateTime Date { get; set; }

        public int? OrderSeqno { get; set; }

        public double Total { get; set; }

        public IEnumerable<AbcoInvoiceLineDto> Lines { get; set; }
    }
}