﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbcoApiSample.Models
{
    public class FilteredList<T>
    {
        public IEnumerable<T> List { get; set; }

        public Metadata Metadata { get; set; }
    }
}
