﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbcoApiSample.Models
{
    public class AbcoInvoiceLineDto
    {
        public string Stockcode { get; set; }

        public double Quantity { get; set; }

        public double LineTotal { get; set; }
    }
}
