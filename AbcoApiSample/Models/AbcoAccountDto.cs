﻿namespace AbcoApiSample.Models
{
    public class AbcoAccountDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string PunchoutUrl { get; set; }

        public string Reference1 { get; set; }
    }
}
