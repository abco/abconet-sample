﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbcoApiSample.Models
{
    public class AbcoOrderLineDto
    {
        public int Id { get; set; }

        public double Quantity { get; set; }

        public string Stockcode { get; set; }

        public double UnitPrice { get; set; }

        public double LineTotal { get; set; }

        public string Description { get; set; }

        public double BestPrice { get; set; }
    }
}
