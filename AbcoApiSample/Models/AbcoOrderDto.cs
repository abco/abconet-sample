﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbcoApiSample.Models
{
    public class AbcoOrderDto
    {
        public int Id { get; set; }

        public double SubTotal { get; set; }

        public double TaxTotal { get; set; }

        public double OrderTotal { get; set; }

        public string Status { get; set; }

        public string ProcessingStatus { get; set; }

        public int ContactSeqno { get; set; }

        public AbcoAccountDto Account { get; set; }

        public AbcoAddressDto Address { get; set; }
        public AbcoAddressDto BillTo { get; set; }

        public IEnumerable<AbcoOrderLineDto> Lines { get; set; }

        public string CustomOrderId { get; set; }


    }
}
