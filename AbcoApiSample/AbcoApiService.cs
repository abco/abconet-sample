﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AbcoApiSample.Models;
using Newtonsoft.Json;

namespace AbcoApiSample
{
    public class AbcoApiService
    {
        private readonly string _baseUrl;
        private readonly HttpClient _client = new HttpClient();

        public AbcoApiService()
        {
            _baseUrl = "https://demo.abconet.com.au"; // Set this in your config WebConfigurationManager.AppSettings["AbcoApiUrl"];

            var bearerToken = "SET YOUR BEARER TOKEN"; //Demo

            _client.DefaultRequestHeaders.Add("Authorization", $"Bearer {bearerToken}" );
        }

        public object PostData(string apiPath, object data)
        {
            return PostData<object>(apiPath, data);
        }

        public T PostData<T>(string apiPath, object data)
        {
            var postData = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
            var result = _client.PostAsync(_baseUrl + apiPath, postData).Result;
            var resultContent = result.Content.ReadAsStringAsync().Result;
            try
            {
                result.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                var error = JsonConvert.DeserializeObject<AbcoErrorDto>(resultContent);
                throw new Exception(error.Message + error.ExceptionMessage, ex);
            }

            return JsonConvert.DeserializeObject<T>(resultContent);
        }

        public object PutData(string apiPath, object data)
        {
            return PutData<object>(apiPath, data);
        }

        public T PutData<T>(string apiPath, object data)
        {
            var postData = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
            var result = _client.PutAsync(_baseUrl + apiPath, postData).Result;
            var resultContent = result.Content.ReadAsStringAsync().Result;
            try
            {
                result.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                throw new Exception("Error connecting to the Abco API", ex);
            }

            return JsonConvert.DeserializeObject<T>(resultContent);
        }

        public T GetData<T>(string apiPath, object paramaters)
        {
            var properties = from p in paramaters.GetType().GetProperties()
                             where p.GetValue(paramaters, null) != null
                             select p.Name + "=" + HttpUtility.UrlEncode(p.GetValue(paramaters, null).ToString());

            // queryString will be set to "Id=1&State=26&Prefix=f&Index=oo"                  
            string queryString = String.Join("&", properties.ToArray());

            return GetData<T>(apiPath + "?" + queryString);
        }


        public object GetData(string apiPath)
        {
            return GetData<object>(apiPath);
        }

        public T GetData<T>(string apiPath)
        {
            HttpResponseMessage response;
            string result;
            try
            {
                response = _client.GetAsync(_baseUrl + apiPath).Result;
                result = response.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex)
            {

                throw new Exception("Error connecting to API.", ex);
            }

            try
            {
                response.EnsureSuccessStatusCode();

                return JsonConvert.DeserializeObject<T>(result);
            }
            catch (Exception ex)
            {
                throw new Exception("Error connecting to API.", ex);
            }

        }
    }
}
