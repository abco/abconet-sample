﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbcoApiSample.Models;
using AbcoApiSample.Services;

namespace AbcoApiSample
{
    class Program
    {
        static void Main(string[] args)
        {
            var addressService = new AddressService();
            var addressList = addressService.GetAddressList();

            var transactionService = new TransactionService();
            var invoiceList = transactionService.GetInvoiceList();
        }
    }
}
