﻿using AbcoApiSample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbcoApiSample.Services
{
    public class TransactionService
    {

        public IEnumerable<AbcoInvoiceDto> GetInvoiceList(DateTime? startDate = null)
        {
            var abcoApi = new AbcoApiService();

            if (!startDate.HasValue)
            {
                startDate = DateTime.Now.AddDays(-7);
            }

            // Note that you can pass in the parameters as second argument.
            // The list of parameters can be seen on our api endpoint (https://demo.abconet.com.au/swagger) on this example, go to /api/transaction
            // This api has 3 optional parameters.
            var invoiceList = abcoApi.GetData<FilteredList<AbcoInvoiceDto>>("/api/transaction", new { startDate }).List;

            return invoiceList;
        }
    }
}
