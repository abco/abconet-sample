﻿using AbcoApiSample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbcoApiSample.Services
{
    public class AddressService
    {
        public IEnumerable<AbcoAddressDto> GetAddressList()
        {
            var abcoApi = new AbcoApiService();

            var addressList = abcoApi.GetData<FilteredList<AbcoAddressDto>>("/api/address?itemsPerPage=10").List;

            return addressList;
        }
    }
}
